const colors = require('tailwindcss/colors')

const palette = (variableName) => {
    return {
        100: `rgba(var(${variableName}), .1)`,
        200: `rgba(var(${variableName}), .2)`,
        300: `rgba(var(${variableName}), .3)`,
        400: `rgba(var(${variableName}), .4)`,
        500: `rgba(var(${variableName}), .5)`,
        600: `rgba(var(${variableName}), .6)`,
        700: `rgba(var(${variableName}), .7)`,
        800: `rgba(var(${variableName}), .8)`,
        900: `rgba(var(${variableName}), .9)`,
        main: `rgba(var(${variableName}), 1)`,
    }
}

/** @type {import('tailwindcss').Config} */
export default {
    darkMode: 'class',
    content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
    theme: {
        extend: {
            fontFamily: {
                tajawal: ['Tajawal'],
                poppins: ['Poppins'],
            },
            colors: {
                dark: '#0d0316',
                primary: palette('--color-primary'),
                secondary: palette('--color-secondary'),
                warning: colors.orange,
                error: colors.red,
                success: colors.green,
            },
        },
    },
    plugins: [require('tailwind-scrollbar')],
}
