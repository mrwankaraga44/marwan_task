import { ToastContainerProps } from 'react-toastify'

import { Direction, Language, Mode, TPalette } from '@types'

import { DEFAULT_PRIMARY_COLOR, DEFAULT_SECONDARY_COLOR } from './default'

type ThemeConfig = {
    language: Language
    mode: Mode
    direction: Direction
    toastSettings: ToastContainerProps & React.RefAttributes<HTMLDivElement>
    palette: TPalette
}

const themeConfig: ThemeConfig = {
    language: Language.AR,
    mode: Mode.LIGHT,
    direction: Direction.RTL,
    toastSettings: {
        newestOnTop: true,
        draggablePercent: 50,
        limit: 5,
        theme: 'colored',
        hideProgressBar: true,
        closeButton: false,
        icon: true,
    },
    palette: {
        primary: DEFAULT_PRIMARY_COLOR,
        secondary: DEFAULT_SECONDARY_COLOR,
    },
}

export default themeConfig
