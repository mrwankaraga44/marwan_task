import { AxiosError } from 'axios'
import { useRef } from 'react'
import { useTranslation } from 'react-i18next'
import {
    MutationFunction,
    MutationKey,
    useMutation,
    useQueryClient,
} from 'react-query'
import { toast } from 'react-toastify'

import { CustomUseMutationOptions } from '@types'

export const useCustomMutation = <
    TVariable = any,
    TData = any,
    TError = TData,
    //   IErrorsResponse
>(
    mutationKey: MutationKey,
    mutationFn: MutationFunction<TData, TVariable>,
    options?: CustomUseMutationOptions<TData, AxiosError<TError>, TVariable>,
) => {
    const { t } = useTranslation()
    const queryClient = useQueryClient()

    const toastId = useRef(null)

    const notify = () =>
        (toastId.current = toast.loading(t('PROCESSING'), { autoClose: false }))

    const update = (type: 'success' | 'error') => {
        let message =
            type === 'success'
                ? options?.promise?.message?.onSuccess
                : options?.promise?.message?.onError

        if (!options?.promise?.withoutTranslation && message)
            message = t(message)

        return (
            toastId &&
            toast.update(toastId.current, {
                render: message,
                type,
                autoClose: 3000,
                isLoading: false,
            })
        )
    }

    const onMutate = async (variables: TVariable) => {
        if (
            !options?.withoutToastMessage &&
            options?.toastMessageType === 'promise'
        )
            notify()
        if (options?.onMutate) options.onMutate(variables)
    }

    const onSuccess = async (data: any, variables: any, context: unknown) => {
        try {
            if (!options?.withoutToastMessage) {
                if (options?.toastMessageType === 'promise') {
                    update('success')
                } else if (data?.messages.length) {
                    const messages: string[] = data.messages
                    // Toast(messages, {
                    //     type: 'success',
                    //     withoutTranslation: true,
                    // })
                }
            }

            if (options?.onSuccess)
                await options.onSuccess(data, variables, context)

            if (options?.invalidateQueries)
                await queryClient.invalidateQueries(options.invalidateQueries)
            if (options?.refetchQueries)
                options.refetchQueries.map((rQ) =>
                    queryClient.refetchQueries(rQ?.filters, rQ?.options),
                )

            // queryClient.refetchQueries()
        } catch (errors) {
            console.log(
                '🚀 ~ file: useMutation.tsx:86 ~ onSuccess ~ errors:',
                errors,
            )
        }
    }

    const onError = async (
        error: AxiosError<any>,
        variables: any,
        context: unknown,
    ): Promise<void> => {
        try {
            if (!options?.withoutToastMessage) {
                if (options?.toastMessageType === 'promise') {
                    update('error')
                } else if (error?.response?.data?.messages) {
                    const messages: string[] = error.response.data.messages
                    // Toast(messages, { type: 'error', withoutTranslation: true })
                }
            }
            if (options?.onError)
                await options.onError(error, variables, context)
        } catch (errors) {
            console.log('🚀 ~ file: useMutation.tsx:103 ~ errors:', errors)
        }
    }
    const mutation = useMutation<TData, AxiosError<TError>, TVariable>(
        mutationKey,
        mutationFn,
        {
            ...options,
            onError,
            onSuccess,
            onMutate,
        },
    )

    return mutation
}
