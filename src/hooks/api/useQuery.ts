import { AxiosError } from 'axios'
import { QueryFunction, QueryKey, UseQueryResult, useQuery } from 'react-query'

import { Toast } from '@components'
import { CustomUseQueryOptions, IBaseResponse } from '@types'

export const useCustomQuery = <
    TData = any,
    TQueryKey extends QueryKey = QueryKey,
>(
    queryKey: TQueryKey,
    queryFn: QueryFunction<TData, TQueryKey>,
    options?: CustomUseQueryOptions<TData>,
): UseQueryResult<TData, AxiosError<any>> => {
    const onSuccess = (response: any) => {
        if (options?.onSuccess) options.onSuccess(response)
    }

    const onError = async (error: AxiosError<IBaseResponse>) => {
        const messages = error?.response?.data?.messages
        if (messages)
            Toast(messages, { type: 'error', withoutTranslation: true })
        if (options?.onError) options.onError(error)
    }

    const query = useQuery<TData, AxiosError<any>, TData, TQueryKey>(
        queryKey,
        queryFn,
        {
            // enabled: false,
            refetchInterval: false,
            refetchOnMount: 'always',
            refetchOnReconnect: 'always',
            refetchOnWindowFocus: false,
            ...options,
            onSuccess,
            onError,
        },
    )

    return query
}
