import { Mode, Palette } from '@types'

import { useSettings } from './uesSettings'

export const useTheme = () => {
    const { settings, saveSettings } = useSettings()

    const changeDocumentMode = (mode: Mode) => {
        if (mode === Mode.DARK) {
            document.documentElement.classList.add(Mode.DARK)
        } else {
            document.documentElement.classList.remove(Mode.DARK)
        }
    }

    const changeDocumentPalette = (hexColor: string, colorType: Palette) => {
        document.documentElement.style.setProperty(
            `--color-${colorType}`,
            hexColor,
        )
    }

    const componentToHex = (c: number) => {
        var hex = c.toString(16)
        return hex.length == 1 ? '0' + hex : hex
    }

    const rgbToHex = (rgbColor: string) => {
        const [r, g, b] = rgbColor.split(',').map((c) => +c.trim())
        return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b)
    }

    const hexToRgb = (hexColor: string) => {
        const r = parseInt(hexColor.substring(1, 3), 16)
        const g = parseInt(hexColor.substring(3, 5), 16)
        const b = parseInt(hexColor.substring(5, 7), 16)
        const newColor = `${r}, ${g}, ${b}`
        return newColor
    }

    const changeMode = (mode: Mode) => {
        changeDocumentMode(mode)
        saveSettings({
            ...settings,
            mode: mode,
        })
    }

    const changeColorPalette = (hexColor: string, colorType: Palette) => {
        const color = hexToRgb(hexColor)
        changeDocumentPalette(color, colorType)
        saveSettings({
            ...settings,
            palette: { ...settings.palette, [colorType]: color },
        })
    }

    const initTheme = () => {
        changeDocumentMode(settings.mode)
    }

    const initPalette = () => {
        changeDocumentPalette(settings.palette.primary, Palette.PRIMARY)
        changeDocumentPalette(settings.palette.secondary, Palette.SECONDARY)
    }

    return {
        mode: settings.mode,
        palette: settings.palette,
        changeMode,
        changeDocumentMode,
        initTheme,
        initPalette,
        componentToHex,
        rgbToHex,
        hexToRgb,
        changeColorPalette,
        changeDocumentPalette,
    }
}
