import { useContext } from 'react'

import { SettingsContext, SettingsContextValue } from '@context'

export const useSettings = (): SettingsContextValue =>
    useContext(SettingsContext)
