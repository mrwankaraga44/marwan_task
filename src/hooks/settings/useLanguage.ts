import { Language } from '@types'
import { useTranslation } from 'react-i18next'

export const useLanguage = () => {
    const { t, i18n } = useTranslation()

    const setFontFamily = (fontFamily?: string) => {
        document.documentElement.style.setProperty(
            `--font-family`,
            fontFamily ?? i18n.language === Language.AR ? 'tajawal' : 'poppins',
        )
    }

    const setDirection = () => {
        document.documentElement.dir = i18n.dir()
    }

    const initLanguage = () => {
        setDirection()
        setFontFamily()
    }

    const changeLanguage = (language: Language) => {
        i18n.changeLanguage(language)
        setDirection()
        setFontFamily()
    }

    return {
        t,
        i18n,
        initLanguage,
        language: i18n.language,
        changeLanguage,
    }
}
