import { useState } from 'react'
import { useQueryClient } from 'react-query'
import { useNavigate } from 'react-router-dom'

import { request } from '@services/request'
import { ApiRoutes, IBaseResponse, PageRoute, StaticKey } from '@types'
import { join } from '@utils'

import { useCustomMutation } from './api'

export const useAuth = () => {
    const queryClient = useQueryClient()
    const navigate = useNavigate()

    const [user, setUser] = useState(null)
    const [accessToken, setAccessToken] = useState(null)
    const [refreshToken, setRefreshToken] = useState(null)

    const loginMutation = useCustomMutation<IBaseResponse, any>(
        'login',
        (data) =>
            request({
                url: ApiRoutes.LOGIN,
                method: 'POST',
                data,
            }),
        {
            onSuccess: ({ data }) => {
                login(data)
                navigate(join(true, PageRoute.HOME), {
                    replace: true,
                })
            },
        },
    )

    const login = (data: any) => {
        const { user, accessToken, refreshToken } = data
        setUser(user)
        setAccessToken(accessToken)
        setRefreshToken(refreshToken)
        localStorage.setItem(StaticKey.USER, JSON.stringify(user))
        localStorage.setItem(StaticKey.ACCESS_TOKEN, accessToken)
    }

    const logout = () => {
        setUser(null)
        setAccessToken(null)
        setRefreshToken(null)
        localStorage.removeItem(StaticKey.USER)
        localStorage.removeItem(StaticKey.ACCESS_TOKEN)
        queryClient.clear()
        navigate(join(PageRoute.AUTH))
    }

    return {
        login,
        logout,
        user,
        accessToken,
        refreshToken,
        loginMutation,
    }
}
