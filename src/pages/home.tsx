import { Table, TableHeader, TableRow, TableCell, BaseModal } from '@components'
import { useEffect, useState } from 'react'
import { Dialog, DialogPanel, DialogTitle, Field } from '@headlessui/react'
import { Controller, useForm } from 'react-hook-form'
import { Input } from '@components/input'
import classNames from 'classnames'
import HtmlEditor from '@components/htmlEditor'
import ReactQuill from 'react-quill'
import { Button } from '@components/button'
import { formDataModel } from '../types/fake-data'
import { DataForm } from '../default-data'
import { v4 as uuidv4 } from 'uuid'
import EditModal from '@components/editCom/inde'
const dataHeader = [
    {
        title: 'اسم النموذج',
    },
    { title: 'وصف النموذج' },
    { title: 'الحالة' },
    { title: 'محتوى النموذج' },
    { title: '' },
    ,
]
console.log('🚀 ~ dataHeader:', dataHeader)
export const Home = () => {
    const [isOpen, setIsOpen] = useState<boolean>(false)
    const [formData, setFormData] = useState([])
    const [editModal, seEditModal] = useState<boolean>(false)
    const [selectedItemIndex, setSelectedItemIndex] = useState(null)
    const [selectedItem, setSelectedItem] = useState(null)
    const [isOpenDeleteModal, setOpenDeleteModal] = useState<boolean>(false)
    const [isOpenEditingModal, setIsEditingModal] = useState<boolean>(false)

    const {
        control,
        formState: { errors },
        reset,
        handleSubmit,
        watch,
    } = useForm<formDataModel, any, formDataModel>({
        defaultValues: DataForm,
        mode: 'onSubmit',
    })

    const onSubmit = (data: any) => {
        setFormData((prevData) => [...prevData, data])
        handleClose()
    }

    const handleClose = () => {
        setIsOpen(false)
        reset()
    }

    const handleCloseEditModal = () => {
        seEditModal(false)
        setSelectedItem(null)
        setSelectedItemIndex(null)
    }

    const handleCloseDeleteModal = () => {
        setOpenDeleteModal(false)
    }

    const handleShow = (index) => {
        setSelectedItem(formData[index])
        setSelectedItemIndex(index)

        seEditModal(true)
    }

    const handleRemoveItem = (index: number) => {
        console.log('🚀 ~ handleRemoveItem ~ index:', index)
        setOpenDeleteModal(true)
        setSelectedItem(formData[index])
        setSelectedItemIndex(index)
    }
    const handleConfirmation = (index: number) => {
        if (selectedItemIndex !== null) {
            setFormData((prevData) =>
                prevData.filter((_, i) => i !== selectedItemIndex),
            )
            setOpenDeleteModal(false)
            setSelectedItem(null)
            setSelectedItemIndex(null)
        }
    }

    const handleEditingItem = (index) => {
        setSelectedItem(formData[index])
        setSelectedItemIndex(index)
        setIsEditingModal(true)
    }
    const handleCloseEditingModal = () => {
        setIsEditingModal(false)
        setSelectedItem(null)
        setSelectedItemIndex(null)
        reset()
    }
    const onSubmitEdit = (data) => {
        if (selectedItemIndex !== null) {
            setFormData((prevData) => {
                const newData = [...prevData]
                newData[selectedItemIndex] = {
                    ...newData[selectedItemIndex],
                    ...data,
                }
                return newData
            })
            setIsEditingModal(false)
            setSelectedItem(null)
            setSelectedItemIndex(null)
            reset()
        }
    }
    return (
        <section className="section-container">
            <BaseModal isOpen={isOpen} title="إنشاء نموذج">
                <form onSubmit={handleSubmit(onSubmit)} className="w-full">
                    <div className="mt-4 flex flex-row gap-2">
                        {' '}
                        <Controller
                            name="name"
                            control={control}
                            rules={{ required: true }}
                            render={(field) => (
                                <Input
                                    {...field}
                                    fullWidth
                                    required
                                    label="اسم النموذج"
                                    error={Boolean(errors?.name)}
                                    inputClassName={classNames(
                                        '!bg-white !border !border-gray-400',
                                    )}
                                />
                            )}
                        ></Controller>
                        <Controller
                            name="lname"
                            control={control}
                            rules={{ required: true }}
                            render={(field) => (
                                <Input
                                    {...field}
                                    fullWidth
                                    required
                                    label="وصف النموذج"
                                    error={Boolean(errors?.name)}
                                    inputClassName={classNames(
                                        '!bg-white !border !border-gray-400',
                                    )}
                                />
                            )}
                        ></Controller>
                    </div>

                    <Controller
                        name="content"
                        control={control}
                        rules={{ required: true }}
                        render={({ field: { onChange, value } }) => (
                            <HtmlEditor value={value} onChange={onChange} />
                        )}
                    />

                    <div className="mt-4 flex flex-row items-start justify-end gap-4">
                        <Button className="" type="submit" color="success">
                            حفظ
                        </Button>
                        <Button color="warning" onClick={handleClose}>
                            اغلاق
                        </Button>
                    </div>
                </form>
            </BaseModal>

            {selectedItemIndex !== null && (
                <BaseModal
                    isOpen={editModal}
                    /*                     closeModal={handleCloseEditingModal}
                     */ title="عرض النموذج"
                >
                    <div className="flex flex-col items-center justify-center gap-3">
                        <p> {selectedItem?.name}</p>
                        <p> {selectedItem?.lname}</p>{' '}
                        <div
                            dangerouslySetInnerHTML={{
                                __html: selectedItem?.content,
                            }}
                        />
                    </div>

                    <Button
                        className="text-center"
                        onClick={() => seEditModal(false)}
                    >
                        اغلاق
                    </Button>
                </BaseModal>
            )}
            <BaseModal isOpen={isOpenDeleteModal} title=" تأكيد حذف العقد">
                <div className="mt-8 flex flex-row justify-end gap-3">
                    <Button onClick={(index) => handleConfirmation(index)}>
                        نعم
                    </Button>
                    <Button variant="outlined" onClick={handleCloseDeleteModal}>
                        لا
                    </Button>
                </div>
            </BaseModal>
            <EditModal
                selectedItem={selectedItem}
                isOpen={isOpenEditingModal}
                closeModal={handleCloseEditingModal}
                resetForm={selectedItem}
                onSubmitEdit={onSubmitEdit}
            />

            <div className="flex flex-col gap-[2rem]" dir="rtl">
                <div className="flex flex-row justify-between">
                    <h1 className="font-tajawal text-2xl font-bold text-red-500 ">
                        نماذج العقود
                    </h1>
                    <Button
                        /*                         className="rounded-lg bg-green-700 px-3 py-2"
                         */ color="success"
                        onClick={() => setIsOpen(true)}
                    >
                        إنشاء نموذج
                    </Button>
                </div>

                <table className="w-full overflow-scroll">
                    <TableHeader>
                        {dataHeader?.map((item) => (
                            <TableCell isHeader>{item?.title}</TableCell>
                        ))}
                    </TableHeader>
                    <tbody className="w-full">
                        {formData.map((item, index) => (
                            <TableRow className="" key={index}>
                                <TableCell>{item.name}</TableCell>
                                <TableCell>{item.lname}</TableCell>
                                <TableCell>
                                    <div
                                        dangerouslySetInnerHTML={{
                                            __html: item?.content,
                                        }}
                                    />
                                </TableCell>
                                <TableCell>
                                    <Button onClick={() => handleShow(index)}>
                                        عرض
                                    </Button>
                                </TableCell>
                                <TableCell>
                                    <div className=" flex flex-col gap-2 md:flex-row">
                                        <Button
                                            className=""
                                            fullWidth
                                            color="error"
                                            onClick={() =>
                                                handleRemoveItem(index)
                                            }
                                        >
                                            حذف
                                        </Button>
                                        <Button
                                            fullWidth
                                            color="success"
                                            onClick={() =>
                                                handleEditingItem(index)
                                            }
                                        >
                                            تعديل
                                        </Button>
                                    </div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </tbody>
                </table>
            </div>
        </section>
    )
}
