import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { useLanguage } from '@hooks/settings'
import { useAuth } from '@hooks/useAuth'
import { PageRoute, Translations } from '@types'

export function NotFound(): JSX.Element {
    const { t } = useLanguage()
    const { accessToken } = useAuth()

    const [url, setUrl] = useState(`/${PageRoute.AUTH}`)

    useEffect(() => {
        setUrl(accessToken ? `/${PageRoute.HOME}` : `/${PageRoute.AUTH}`)
    }, [])

    return (
        <>
            <title>{t(Translations.PAGES.PAGE_NOT_FOUND)}</title>
            <div className="flex h-screen w-screen items-center justify-center bg-cover">
                <div className="text-center">
                    <div className="cursor-default select-none rounded-md bg-primary-500 px-10 py-5 shadow-md">
                        <h1 className="text-5xl font-bold text-white">
                            {t(Translations.PAGES.PAGE_NOT_FOUND)}
                        </h1>
                        <p className="mt-5 text-5xl font-bold tracking-widest text-white">
                            404
                        </p>
                    </div>
                    <div className="mt-5 rounded-md bg-primary-500 text-lg font-medium text-white shadow-md transition-all duration-200 hover:bg-primary-600">
                        <Link to={url} className="block p-3">
                            {t(Translations.COMMON.RETURN_TO, {
                                destination: accessToken
                                    ? Translations.PAGES.HOME
                                    : Translations.PAGES.LOGIN,
                            })}
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}
