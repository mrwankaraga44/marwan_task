export enum PageRoute {
    NOT_FOUND = 'Not-Found',

    AUTH = 'Auth',
    LOGIN = 'Login',

    HOME = 'Home',
    SETTINGS = 'Settings',

    ADD = 'Add',
    EDIT = 'Edit',
    SHOW = 'Show',
}
