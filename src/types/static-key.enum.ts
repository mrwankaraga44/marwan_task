export enum StaticKey {
    USER = 'user',
    I18N_LNG = 'i18nextLng',
    ACCESS_TOKEN = 'accessToken',
}
