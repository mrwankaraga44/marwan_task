import { ReactNode } from 'react'

import { Color, Variant } from './theme'

export type BaseProps = {
    variant?: Variant
    color?: Color
    fullWidth?: boolean
    error?: boolean
    errortext?: string
    loading?: boolean
    icon?: ReactNode | string
}
