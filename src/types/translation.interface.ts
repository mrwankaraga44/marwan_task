import { TOptionsBase } from 'i18next'

export interface ITranslateOptions extends TOptionsBase {
    [key: string]: any
}

export interface ITranslationProps {
    withoutTranslation?: boolean
    i18nOptions?: ITranslateOptions
}
