export enum Palette {
    PRIMARY = 'primary',
    SECONDARY = 'secondary',
}

export type TPalette = {
    primary: string
    secondary: string
}
