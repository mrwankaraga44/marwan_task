import { AxiosError } from 'axios'
import { UseQueryOptions } from 'react-query'

export interface CustomUseQueryOptions<TData = any>
    extends Omit<
        UseQueryOptions<TData, AxiosError<any>>,
        'queryFn' | 'queryKey'
    > {}
