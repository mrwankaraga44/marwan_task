export interface IBaseResponse<TData = any, TMessages = string[]> {
  status: boolean
  code: number
  data?: TData
  messages: TMessages
}
