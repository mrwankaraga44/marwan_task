import {
    QueryKey,
    RefetchOptions,
    RefetchQueryFilters,
    UseMutationOptions,
} from 'react-query'

export interface CustomUseMutationOptions<
    TData = any,
    TError = any,
    TVariable = any,
> extends Omit<
        UseMutationOptions<TData, TError, TVariable>,
        'mutationKey' | 'mutationFn'
    > {
    invalidateQueries?: QueryKey
    refetchQueries?: {
        filters?: RefetchQueryFilters
        options?: RefetchOptions
    }[]
    withoutToastMessage?: boolean
    toastMessageType?: 'promise' | 'default'
    promise?: {
        message?: {
            onLoading?: string
            onSuccess?: string
            onError?: string
        }
        withoutTranslation?: boolean
    }
    // refetchQueries<TPageData = unknown>(
    //   filters?: RefetchQueryFilters<TPageData>,
    //   options?: RefetchOptions
    // ): Promise<void>
    // refetchQueries<TPageData = unknown>(
    //   queryKey?: QueryKey,
    //   filters?: RefetchQueryFilters<TPageData>,
    //   options?: RefetchOptions
    // ): Promise<void>
}
