// ** React Imports
import { ReactNode, createContext, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'

import themeConfig from '@config/themeConfig'
import { Direction, Language, Mode, TPalette } from '@types'

export type Settings = {
    language: Language
    mode: Mode
    direction: Direction
    palette: TPalette
}

export type SettingsContextValue = {
    settings: Settings
    saveSettings: (updatedSettings: Settings) => void
}

interface SettingsProviderProps {
    children: ReactNode
}

const initialSettings: Settings = {
    language: Language.AR,
    mode: themeConfig.mode,
    direction: themeConfig.direction,
    palette: themeConfig.palette,
}

const staticSettings = {}

const restoreSettings = (): Settings | null => {
    let settings = null

    try {
        const storedData: string | null = localStorage.getItem('settings')

        if (storedData) {
            settings = { ...JSON.parse(storedData), ...staticSettings }
        } else {
            settings = initialSettings
        }
    } catch (err) {
        console.error(err)
    }

    return settings
}

const storeSettings = (settings: Settings) => {
    const initSettings = Object.assign({}, settings)
    localStorage.setItem('settings', JSON.stringify(initSettings))
}

export const SettingsContext = createContext<SettingsContextValue>({
    saveSettings: () => null,
    settings: initialSettings,
})

export const SettingsProvider = ({ children }: SettingsProviderProps) => {
    const { i18n } = useTranslation()
    const [settings, setSettings] = useState<Settings>(initialSettings)

    useEffect(() => {
        const restoredSettings = restoreSettings()
        saveSettings({
            ...restoredSettings,
            language: i18n.language as Language,
            direction: i18n.dir() as Direction,
        })
    }, [i18n.language])

    const saveSettings = (updatedSettings: Settings) => {
        storeSettings(updatedSettings)
        setSettings(updatedSettings)
    }

    return (
        <SettingsContext.Provider value={{ settings, saveSettings }}>
            {children}
        </SettingsContext.Provider>
    )
}

export const SettingsConsumer = SettingsContext.Consumer
