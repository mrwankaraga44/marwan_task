import { Navigate, Route, Routes } from 'react-router-dom'

import { useAuth } from '@hooks/useAuth'
import { PageRoute } from '@types'

import { Home, NotFound } from '@pages'

const App = () => {
    const { accessToken } = useAuth()

    return (
        <Routes>
            {/*  <Route
                path="/"
                element={
                    <Navigate
                        to={accessToken ? PageRoute.HOME : PageRoute.HOME}
                    />
                }
            /> */}
            <Route path={`/`} element={<Home />} />
            <Route path="*" element={<NotFound />} />
        </Routes>
    )
}

export default App
