import { ReactNode } from 'react'
import { ToastContainer } from 'react-toastify'

import themeConfig from '@config/themeConfig'
import { useLanguage } from '@hooks/settings'
import { Language } from '@types'

type ToastLayoutProps = { children: ReactNode }

export const ToastLayout = ({ children }: ToastLayoutProps) => {
    const { i18n } = useLanguage()

    return (
        <>
            <ToastContainer
                {...themeConfig.toastSettings}
                position={
                    i18n.language === Language.AR ? 'top-right' : 'top-left'
                }
                rtl={i18n.language === Language.AR}
            />
            {children}
        </>
    )
}
