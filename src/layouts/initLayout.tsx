import { ReactNode, useEffect } from 'react'

import { useLanguage, useSettings, useTheme } from '@hooks/settings'

type InitLayoutProps = {
    children: ReactNode
}

export const InitLayout = ({ children }: InitLayoutProps) => {
    const { initTheme, initPalette } = useTheme()
    const { initLanguage, language } = useLanguage()
    const { settings } = useSettings()

    useEffect(() => {
        initTheme()
    }, [settings.mode])

    useEffect(() => {
        initPalette()
    }, [settings.palette])

    useEffect(() => {
        initLanguage()
    }, [language])

    return <>{children}</>
}
