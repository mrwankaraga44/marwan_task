type JoinOverload = {
    (...paths: string[]): string
    (firstWithSeparator: boolean, ...paths: string[]): string
}

export const join: JoinOverload = (...params: any): string => {
    var path = ''
    if (typeof params[0] === 'boolean') {
        const [firstWithSeparator, ...paths] = params
        paths.forEach((p: string, index: number) => {
            if (!firstWithSeparator && index === 0) path += `${p}`
            else path += `/${p}`
        })
    } else params.forEach((p: string) => (path += `/${p}`))
    return path
}
