import classNames from 'classnames'
import { DetailedHTMLProps, InputHTMLAttributes, forwardRef } from 'react'
import {
    ControllerFieldState,
    ControllerRenderProps,
    UseFormStateReturn,
} from 'react-hook-form'

import { BaseProps } from '../../types/base/base-props.type'
import { Trans } from 'react-i18next'
import { useLanguage } from '@hooks/settings'

export type InputProps<FieldType = any> = DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
> &
    BaseProps & {
        field?: ControllerRenderProps<FieldType, any>
        fieldState?: ControllerFieldState
        formState?: UseFormStateReturn<FieldType>
        label?: string
        withLabel?: boolean
        withErrorLabel?: boolean
        withoutTranslate?: boolean
        inputClassName?: string
    }

export const Input = forwardRef<HTMLInputElement, InputProps>(
    (
        {
            className,
            inputClassName,
            fullWidth,
            label,
            withLabel,
            field,
            fieldState,
            formState,
            withErrorLabel = true,
            withoutTranslate = false,
            error,
            placeholder,
            ...props
        },
        ref,
    ) => {
        const { t } = useLanguage()

        return (
            <div
                className={classNames(
                    className,
                    'flex flex-col',
                    props?.type === 'checkbox' && 'items-center gap-2',
                )}
            >
                {(withLabel || label) && (
                    <label
                        htmlFor={field?.name ?? props.name}
                        className={classNames(
                            'w-fit text-start text-sm text-gray-700 dark:text-white',
                        )}
                    >
                        {!withoutTranslate ? (
                            <Trans>{label ?? field?.name ?? props.name}</Trans>
                        ) : (
                            label ?? field?.name ?? props.name
                        )}
                    </label>
                )}
                <input
                    className={classNames(
                        fullWidth && 'w-full',
                        'mt-1 rounded-md border-2 border-transparent bg-gray-200 p-4 text-sm outline-none dark:text-white',
                        error && '!border-error-500',
                        'focus:!border-primary-main',
                        props?.type === 'checkbox' && 'h-5 w-5',
                        inputClassName,
                        'dark:!bg-transparent',
                    )}
                    id={field?.name ?? props.name}
                    {...field}
                    placeholder={
                        !withoutTranslate ? t(placeholder) : placeholder
                    }
                    disabled={formState?.isSubmitting}
                    {...props}
                    ref={ref}
                />
                {withErrorLabel && (
                    <small
                        className={classNames(
                            !fieldState?.error?.message && 'invisible',
                            'mx-1 text-error-500',
                        )}
                    >
                        {fieldState?.error?.message ?? '.'}
                    </small>
                )}
            </div>
        )
    },
)
