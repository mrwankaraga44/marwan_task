import { Fragment, ReactNode } from 'react'
import { Dialog, Transition } from '@headlessui/react'

interface IBaseModalProps {
    closeModal?: () => void
    isOpen: boolean
    children: ReactNode
    title?: string
}

export const BaseModal = ({
    closeModal,
    isOpen,
    children,
    title,
}: IBaseModalProps) => {
    return (
        <Transition appear show={isOpen} as={Fragment}>
            <Dialog
                dir="rtl"
                as="div"
                className="relative z-10"
                is="div"
                onClose={closeModal ? closeModal : () => {}}
            >
                <Transition.Child
                    as={Fragment}
                    enter="ease-in-out duration-200"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <div className="fixed inset-0 bg-black/50" />
                </Transition.Child>
                <div className="fixed inset-0">
                    <div className="flex min-h-full items-center justify-center p-0 text-center">
                        <Dialog.Panel className="base-scroll  w-11/12 max-w-md transform rounded-xl bg-white p-6 text-left align-middle shadow-xl">
                            <Dialog.Title dir="rtl" className={'text-right'}>
                                {title}
                            </Dialog.Title>
                            {children}
                        </Dialog.Panel>
                    </div>
                </div>
            </Dialog>
        </Transition>
    )
}
