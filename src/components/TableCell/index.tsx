export const TableCell = ({
    children,
    isHeader,
}: {
    children: any
    isHeader: any
}) => {
    const classes = 'py-2 px-4 border-b text-center'
    return isHeader ? (
        <th className={classes}>{children}</th>
    ) : (
        <td className={classes}>{children}</td>
    )
}
