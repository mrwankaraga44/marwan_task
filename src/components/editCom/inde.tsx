import React, { useEffect } from 'react'
import { BaseModal } from '../modal'
import { Button } from '../button'
import { useForm, Controller } from 'react-hook-form'
import { Input } from '../input'
import HtmlEditor from '../htmlEditor'

const EditModal = ({
    isOpen,
    closeModal,
    onSubmitEdit,
    selectedItem,
    resetForm,
}) => {
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm({
        defaultValues: selectedItem,
        mode: 'onSubmit',
    })

    /* const handleClose = () => {
        closeModal()
        resetForm() 
        reset(selectedItem) 
    }
 */

    useEffect(() => {
        reset(selectedItem)
    }, [isOpen, selectedItem, reset])
    return (
        <BaseModal
            isOpen={isOpen}
            closeModal={closeModal}
            title="تعديل النموذج"
        >
            <form onSubmit={handleSubmit(onSubmitEdit)} className="w-full">
                <div className="flex flex-row gap-2">
                    <Controller
                        name="name"
                        control={control}
                        rules={{ required: true }}
                        render={({ field }) => (
                            <Input
                                {...field}
                                fullWidth
                                defaultValue={selectedItem?.name}
                                required
                                label="اسم النموذج"
                                error={Boolean(errors.name)}
                                inputClassName="!bg-white !border !border-gray-400"
                            />
                        )}
                    />
                    <Controller
                        name="lname"
                        control={control}
                        rules={{ required: true }}
                        render={({ field }) => (
                            <Input
                                {...field}
                                fullWidth
                                required
                                defaultValue={selectedItem?.lname}
                                label="وصف النموذج"
                                error={Boolean(errors.name)}
                                inputClassName="!bg-white !border !border-gray-400"
                            />
                        )}
                    />
                </div>

                <Controller
                    name="content"
                    defaultValue={selectedItem?.content}
                    control={control}
                    rules={{ required: true }}
                    render={({ field: { onChange, value } }) => (
                        <HtmlEditor value={value} onChange={onChange} />
                    )}
                />

                <div className="mt-4 flex flex-row items-start justify-end gap-4">
                    <Button className="" type="submit" color="success">
                        حفظ التعديلات
                    </Button>
                    <Button color="warning" onClick={closeModal}>
                        إلغاء
                    </Button>
                </div>
            </form>
        </BaseModal>
    )
}

export default EditModal
