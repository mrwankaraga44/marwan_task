export const Table = ({ children }) => {
    return (
        <div className="overflow-x-auto">
            <table className=" border border-gray-200 bg-white">
                {children}
            </table>
        </div>
    )
}
