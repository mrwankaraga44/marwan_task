export const TableHeader = ({ children }) => {
    return (
        <thead>
            <tr className="w-full bg-gray-100">{children}</tr>
        </thead>
    )
}
