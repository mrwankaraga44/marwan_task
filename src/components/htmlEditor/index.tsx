import React, { useState } from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css' // Import Quill styles
const HtmlEditor = ({ value, onChange }) => {
    /*     const [editorContent, setEditorContent] = useState('')

    const handleChange = (content) => {
        setEditorContent(content)
    } */

    return (
        <div className="html-editor" dir="rtl">
            <ReactQuill
                value={value}
                onChange={onChange}
                modules={HtmlEditor.modules}
                formats={HtmlEditor.formats}
            />
        </div>
    )
}

HtmlEditor.modules = {
    toolbar: [
        [{ header: '1' }, { header: '2' }, { font: [] }],
        [{ size: [] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [
            { list: 'ordered' },
            { list: 'bullet' },
            { indent: '-1' },
            { indent: '+1' },
        ],
        ['link', 'image', 'video'],
        ['clean'],
    ],
}

HtmlEditor.formats = [
    'header',
    'font',
    'size',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'list',
    'bullet',
    'indent',
    'link',
    'image',
    'video',
]

export default HtmlEditor
