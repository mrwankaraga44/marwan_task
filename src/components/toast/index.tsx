import { Trans } from 'react-i18next'
import { ToastOptions as BaseToastOptions, toast } from 'react-toastify'

import { ITranslationProps } from '@types'

type ToastOptions = BaseToastOptions & ITranslationProps

export const Toast = (text: string | string[], options?: ToastOptions) => {
    if (typeof text === 'string')
        return toast(
            options?.withoutTranslation ? (
                text
            ) : (
                <Trans i18nKey={text} {...options.i18nOptions} />
            ),
            { type: 'default', ...options },
        )
    else if (text.length)
        return text.map((message) =>
            toast(
                options?.withoutTranslation ? (
                    message
                ) : (
                    <Trans i18nKey={text} {...options.i18nOptions} />
                ),
                { type: 'default', ...options },
            ),
        )
}
