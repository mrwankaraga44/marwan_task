import classNames from 'classnames'
import { ButtonHTMLAttributes, DetailedHTMLProps } from 'react'

export const Button = ({
    variant = 'contained',
    color = 'primary',
    fullWidth = false,
    children,
    className,
    ...props
}: DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
> & {
    variant?: 'contained' | 'outlined' | 'text'
    color?: 'primary' | 'secondary' | 'error' | 'info' | 'warning' | 'success'
    fullWidth?: boolean
}) => {
    const getTextColor = () => {
        switch (color) {
            case 'primary':
                return '!text-primary-main'
            case 'secondary':
                return '!text-secondary-main'
            case 'error':
                return '!text-error-500'
            case 'info':
                return '!text-info-500'
            case 'warning':
                return '!text-warning-500'
            case 'success':
                return '!text-success-500'
        }
    }

    const getBackground = () => {
        switch (color) {
            case 'primary':
                return 'bg-primary-main hover:bg-primary-800 border border-primary-500 hover:border-primary-600 text-primary-500 hover:text-primary-600  '
            case 'secondary':
                return 'bg-secondary-main hover:bg-secondary-800 border border-secondary-500 hover:border-secondary-600 text-secondary-500 hover:text-secondary-600'
            case 'error':
                return 'bg-error-500 hover:bg-error-600 border border-error-500 hover:border-error-600 text-error-500 hover:text-error-600'
            case 'info':
                return 'bg-info-500 hover:bg-info-600 border border-info-500 hover:border-info-600 text-info-500 hover:text-info-600'
            case 'warning':
                return 'bg-warning-500 hover:bg-warning-600 border border-warning-500 hover:border-warning-600 text-warning-500 hover:text-warning-600'
            case 'success':
                return 'bg-success-500 hover:bg-success-600 border border-success-500 hover:border-success-600 text-success-500 hover:text-success-600'
            default:
                return 'bg-lightGray hover:bg-lightGray/75 border border-lightGray hover:border-lightGray/75 text-lightGray hover:text-lightGray/75'
        }
    }

    const getStyle = () => {
        switch (variant) {
            case 'contained':
                return classNames('!text-white')
            case 'outlined':
                return classNames(
                    '!bg-transparent hover:bg-transparent',
                    getTextColor(),
                )
            case 'text':
                return classNames(
                    '!bg-transparent hover:bg-transparent !border-none',
                )
            default:
                return classNames(className)
        }
    }

    return (
        <button
            {...props}
            className={classNames(
                className,
                getStyle(),
                getBackground(),
                fullWidth ? 'w-full' : 'w-fit',
                'primary-transition text-lightGray rounded-md px-6 py-2 font-bold',
            )}
        >
            {children}
        </button>
    )
}
