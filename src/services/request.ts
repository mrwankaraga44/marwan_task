import axios, { AxiosRequestConfig } from 'axios'

import { StaticKey } from '@types'

interface CustomAxiosRequestConfig<TData> extends AxiosRequestConfig<TData> {
    withToken?: boolean
}

const apiUrl = import.meta.env.VITE_API_URL
const client = axios.create({ baseURL: apiUrl })
export const request = async <TResponse = any, TData = any>({
    withToken = true,
    ...options
}: CustomAxiosRequestConfig<TData>): Promise<TResponse> => {
    const language = localStorage.getItem(StaticKey.I18N_LNG)
    client.defaults.headers.common = {
        'accept-language': language,
    }

    if (withToken) {
        const Authorization = `Bearer ${localStorage.getItem(
            StaticKey.ACCESS_TOKEN,
        )}`
        client.defaults.headers.common.Authorization = Authorization
    }
    return client({ ...options }).then((data) => data.data)
}
