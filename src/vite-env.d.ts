/// <reference types="vite/client" />

interface ImportMetaEnv {
    VITE_API_URL: string
    VITE_API_ROOT: string
}

interface ImportMeta {
    readonly env: ImportMetaEnv
}
