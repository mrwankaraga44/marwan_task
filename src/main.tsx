import { Suspense } from 'react'
import { createRoot } from 'react-dom/client'
import { QueryClient, QueryClientProvider } from 'react-query'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

import { SettingsProvider } from '@context'
import { InitLayout, ToastLayout } from '@layouts'
import { store } from '@store'

import App from './App.tsx'

import './services/i18n.ts'

import './styles/index.scss'

const queryClient = new QueryClient()

createRoot(document.getElementById('root')!).render(
    <Suspense fallback="loading">
        <Provider store={store}>
            <QueryClientProvider client={queryClient}>
                <BrowserRouter>
                    <SettingsProvider>
                        <InitLayout>
                            <ToastLayout>
                                <App />
                            </ToastLayout>
                        </InitLayout>
                    </SettingsProvider>
                </BrowserRouter>
            </QueryClientProvider>
        </Provider>
    </Suspense>,
)
