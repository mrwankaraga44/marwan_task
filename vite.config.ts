import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react()],
    resolve: {
        alias: {
            '@assets': '/src/assets',
            '@components': '/src/components',
            '@config': '/src/config',
            '@context': '/src/context',
            '@hooks': '/src/hooks',
            '@layouts': '/src/layouts',
            '@pages': '/src/pages',
            '@services': '/src/services',
            '@store': '/src/store',
            '@types': '/src/types',
            '@utils': '/src/utils',
            '@views': '/src/views',
        },
    },
})
